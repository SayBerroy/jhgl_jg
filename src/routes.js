import Vue from 'vue'
import VueRouter from 'vue-router'
Vue.use(VueRouter)
import {
  allRoutes
} from '@/config'
import iView from 'iview';
import store from './store'
import {
  storage
} from '@/lib'

var routes = []

allRoutes.forEach(r => {
  //生成一级路由
  var obj = {
    path: r.path,
    meta: {
      title: r.label,

    },
    children: [],
    // component: () => import("@/views/" + r.component)
    component: resolve => require(['@/views/' + r.component], resolve)
  }
  routes.push(obj)
  //生成二级路由
  if (r.sublist) {
    r.sublist.forEach(rr => {
      obj.children.push({
        path: rr.path,
        meta: {
          title: r.label + "-" + rr.label,
          auth: rr.auth
        },
        // component: () => import("@/views/" + rr.component)
        component: resolve => require(['@/views/' + rr.component], resolve)
      })
    })
  }
});

routes.push({
  path: "*",
  component: () => import("@/views/system/404.vue")
})

const router = new VueRouter({
  routes
})

router.beforeEach((to, from, next) => {
  if (to.meta) {
    if (to.path != '/login') {
      if (storage.get('token')) {
        // document.title = to.meta.title
        next()
      } else {
        // document.title = to.meta.title
        next("/login")
      }
    } else {
      // document.title = to.meta.title
      next()
    }
  }
  //需要判断是否登录的页面
  // if (to.meta && to.meta.auth) {
  //     if (!store.state.nickname) {
  //         if(to.path == '/login'){
  //             next();
  //         }else{
  //             next("/login")
  //         }
  //     } else {
  //         iView.LoadingBar.start();
  //         if (to.meta && to.meta.title){
  //             document.title = to.meta.title
  //             }
  //         next(true)
  //     }
  // } else {
  //     iView.LoadingBar.start();
  //     if (to.meta && to.meta.title)
  //         document.title = to.meta.title
  //     next()
  // }
  // if(to.meta){
  //     if(to.path != '/login'){
  //         if(storage.get('token')){
  //             document.title = to.meta.title
  //             next()
  //         }else{
  //             document.title = to.meta.title
  //             next("/login")
  //         }
  //     }else{
  //         document.title = to.meta.title
  //         next()
  //     }

  // }
  // next();
})

router.afterEach((to) => {
  iView.LoadingBar.finish();
  // allRoutes.forEach((r, i) => {
  //     if (r.sublist) {
  //         r.sublist.forEach((rr, j) => {
  //             if (rr.path === to.path) {
  //                 store.commit("setActiveId", {
  //                     menuid: (i + 1).toString(),
  //                     labelid: i + 1 + "-" + (j + 1)
  //                 });
  //             }
  //         });
  //     }
  // });
  store.commit("user/setActiveId", {
    activeid: sessionStorage.getItem('setActiveId')
  });
  if (sessionStorage.getItem('openmenusid')) {
    store.commit('user/setopenmenusId', {
      openmenusid: JSON.parse(sessionStorage.getItem('openmenusid'))
    })
  }
});


export default router
