import Axios from 'axios'
import {
    baseURL
} from '../config'
import iView from 'iview'
import router from '../routes'
var token = sessionStorage.getItem('token')

// Axios.defaults.headers.common['X-Access-Token'] = token;
export const fetch = Axios.create({
    // baseURL,
    timeout: 30000,
})
// 添加请求拦截器
fetch.interceptors.request.use(function (config) {
    // iView.LoadingBar.start();
    config.headers['X-Access-Token']= sessionStorage.getItem('token');
    return config;
}, function (error) {
    // 对请求错误做些什么
    return Promise.reject(error);
});

// 添加响应拦截器
fetch.interceptors.response.use(function (response) {
    // iView.LoadingBar.finish();
    return response;
}, function (error) {
  if(error.response.status == 500){
    iView.Message.error('登录失效，请重新登录');
    sessionStorage.removeItem('token');
    setTimeout(()=>{
      router.push({path:'/login'})
    },1500);
  }
    // 对响应错误做点什么
    return Promise.reject(error);
});
