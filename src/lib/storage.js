export const storage = {
    set(key, val) {
        sessionStorage.setItem(key, val);
    },
    get(key) {
        return sessionStorage.getItem(key);
    },
    remove(key) {
        debugger;
        sessionStorage.removeItem(key);
    }
}