import user from './user'
import account from './account'
import system from './system'
const obj = {
    user,
    system,
    account,
    install(Vue, options) {
        Vue.prototype.$service = this
    }
}



export default obj