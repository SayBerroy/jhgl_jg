export const APIURI = {
    USER: {
        STUDENTLIST: "studentlist"
    },
    ACCOUNT: {
        LOGIN: "gunsApi/auth"
    },
    SYSTEM: {
        COUNT: 'home/count',
        BZSJ: "/home/bzsj",
        BZLXSJ: "/home/bzlxsj"
    }
}