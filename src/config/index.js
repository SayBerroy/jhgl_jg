export {
    APIURI
}
from './apiUri'
export const lang = "cn"
export const baseURL = "/api"
import routes from './menus'
export const allRoutes = routes
    //所有菜单项
export const menuList = routes.filter(r => r.inMenu)