import {
    storage
} from '@/lib'

export default {
    logout(state) {
        state.nickname = ""
        storage.remove("userinfo");
        storage.remove("token");
        storage.remove("account");
    },
    setUserInfo(state, data) {
        storage.set("userinfo", data.nickname);
        storage.set('token',data.token);
        storage.set('account',data.account);
        state.nickname = data.nickname;
        state.token = data.token;
        state.account = data.account;
    },

    setActiveId(state,data){
        state.activeid = data.activeid;
    },
    setSyzt(state,data){
      state.syzt = data.syzt;
    },
    setopenmenusId(state,data){
        state.openmenusid = data.openmenusid;
    }
}
