import {
    storage
} from '@/lib'
export default {
    getNickName(state) {
        return state.nickname || storage.get("userinfo");
    },
    getToken(state) {
        return state.token || storage.get("token");
    }
}